package app.land.flightsearchapp

import android.app.Application

class ApplicationClass: Application() {

    override fun onCreate() {
        super.onCreate()
        App.init(this)
    }

}