package app.land.flightsearchapp.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.land.flightsearchapp.R
import app.land.flightsearchapp.service.repository.DataStatus
import app.land.flightsearchapp.view.adapters.AirlinesListAdapter
import app.land.flightsearchapp.viewmodel.AirlinesViewModel

class AirlinesListFragment: Fragment() {

    companion object{
        const val TAG = "app.land.flightsearchapp.view.fragments.AirlinesListFragment"
        fun newInstance(): AirlinesListFragment {
            val args = Bundle()

            val fragment = AirlinesListFragment()
            fragment.arguments = args
            return fragment
        }
    }

    lateinit var viewModel : AirlinesViewModel
    lateinit var rvAirlinesList: RecyclerView
    lateinit var pbApiLoader: ProgressBar
    private val adapter = AirlinesListAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.f_airlines_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(view)
    }

    private fun initViews(view: View){
        pbApiLoader = view.findViewById(R.id.pb_api_loader)
        rvAirlinesList = view.findViewById(R.id.rv_airlines_list)
        rvAirlinesList.adapter = adapter
        rvAirlinesList.layoutManager = LinearLayoutManager(requireContext())
        rvAirlinesList.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
    }

    private fun setData(){
        showProgressBar()
        viewModel.getAirlinesDisplayList(this).observe(this, {
            if(it.first == DataStatus.NETWORK) {
                adapter.list = it.second.list
                adapter.notifyDataSetChanged()
                hideProgressBar()
            }
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(requireActivity()).get(AirlinesViewModel::class.java)
        setData()
    }

    fun hideProgressBar(){
        pbApiLoader.visibility = View.GONE
    }

    fun showProgressBar(){
        pbApiLoader.visibility = View.VISIBLE
    }


}