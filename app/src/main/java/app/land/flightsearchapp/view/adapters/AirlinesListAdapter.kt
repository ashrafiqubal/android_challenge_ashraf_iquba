package app.land.flightsearchapp.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.land.flightsearchapp.R
import app.land.flightsearchapp.service.model.AirlinesInfoModelClass
import app.land.flightsearchapp.view.viewholders.FlightInfoViewHolder

class AirlinesListAdapter: RecyclerView.Adapter<FlightInfoViewHolder>() {

    var list: List<AirlinesInfoModelClass> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlightInfoViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.vh_airline_item, parent, false)
        return FlightInfoViewHolder(view)
    }

    override fun onBindViewHolder(holder: FlightInfoViewHolder, position: Int) {
        holder.setData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }
}