package app.land.flightsearchapp.view.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import app.land.flightsearchapp.R
import app.land.flightsearchapp.view.BaseActivity
import app.land.flightsearchapp.view.fragments.AirlinesListFragment

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setFragment(AirlinesListFragment.newInstance(), AirlinesListFragment.TAG)
    }

    override fun getContainerId(): Int {
        return R.id.fragment_container
    }


}
