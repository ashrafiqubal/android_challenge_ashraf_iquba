package app.land.flightsearchapp.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import app.land.flightsearchapp.R

public abstract class BaseActivity: AppCompatActivity() {


    abstract fun getContainerId(): Int

    protected fun setFragment(fragment: Fragment, tag: String?){
        val transaction : FragmentTransaction = supportFragmentManager.beginTransaction()
        if(supportFragmentManager.backStackEntryCount > 1){
            transaction.setCustomAnimations(
                R.anim.nb_enter_from_right_slow, R.anim.nb_exit_to_left_slow,
                R.anim.nb_enter_from_left_slow, R.anim.nb_exit_to_right_slow
            )
        }
        transaction.replace(getContainerId(), fragment, tag)
        transaction.addToBackStack(tag)
        transaction.commitAllowingStateLoss()
    }

    fun popAllFragments(){
        supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    fun popCurrentFragment(){
        supportFragmentManager.popBackStack()
    }

    fun popCurrentFragmentImmediate() : Boolean{
        return supportFragmentManager.popBackStackImmediate()
    }

    fun popFragment(TAG : String){
        supportFragmentManager.popBackStack(TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    fun getCurrentFragment() : Fragment?{
        return supportFragmentManager.findFragmentById(R.id.container)
    }

    override fun onBackPressed() {
        if(supportFragmentManager.backStackEntryCount > 1){
            super.onBackPressed()
        }else{
            finish()
        }
    }
}