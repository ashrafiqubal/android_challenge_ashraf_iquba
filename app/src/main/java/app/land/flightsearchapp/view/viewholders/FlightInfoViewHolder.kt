package app.land.flightsearchapp.view.viewholders

import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import app.land.flightsearchapp.R
import app.land.flightsearchapp.service.model.AirlinesInfoModelClass

class FlightInfoViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    private val tvDepTime: TextView = itemView.findViewById(R.id.tv_dep_time)
    private val tvArrivalTime: TextView = itemView.findViewById(R.id.tv_arrival_time)
    private val tvDepAirportName : TextView = itemView.findViewById(R.id.tv_dep_airport_name)
    private val tvArrivalAirportName: TextView = itemView.findViewById(R.id.tv_arrival_airport_name)
    val tvDuration: TextView = itemView.findViewById(R.id.tv_duration)
    val tvFlightClass: TextView = itemView.findViewById(R.id.tv_flight_class)
    val tvLowestFare: TextView = itemView.findViewById(R.id.tv_lowest_fare)

    fun setData(data: AirlinesInfoModelClass){

        if(!TextUtils.isEmpty(data.depTime)){
            tvDepTime.text = data.depTime
        }else{
            tvDepTime.text = ""
        }

        if(!TextUtils.isEmpty(data.arrTime)){
            tvArrivalTime.text = data.arrTime
        }else{
            tvArrivalTime.text = ""
        }

        if(!TextUtils.isEmpty(data.origin)){
            tvDepAirportName.text = data.origin
        }else{
            tvDepAirportName.text = ""
        }

        if(!TextUtils.isEmpty(data.destination)){
            tvArrivalAirportName.text = data.destination
        }else{
            tvArrivalAirportName.text = data.destination
        }

        if(!TextUtils.isEmpty(data.lowestFare)){
            tvLowestFare.text = data.lowestFare
        }else{
            tvLowestFare.text = ""
        }

        if(!TextUtils.isEmpty(data.duration)){
            tvDuration.text = data.duration
        }else{
            tvDuration.text = data.duration
        }

        if(!TextUtils.isEmpty(data.type)){
            tvFlightClass.text = data.type
        }else{
            tvFlightClass.text = ""
        }

        itemView.setOnClickListener {

        }
    }
}