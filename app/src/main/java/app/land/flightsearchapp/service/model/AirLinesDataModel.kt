package app.land.flightsearchapp.service.model

import com.google.gson.annotations.SerializedName

data class AppendixDataModel(val airlines: HashMap<String, String>, val airports: HashMap<String, String>, val providers: HashMap<String, String>)

data class FaresDataClass(val providerId: Int, val fare: Int)

data class FlightsDataClass(val originCode: String, val destinationCode: String, val departureTime: Long, val arrivalTime: Long, val fares: List<FaresDataClass>, val airlineCode: String){
    @SerializedName("class")
    var type: String = ""
}

data class SearchResultsDataModel(val flights: List<FlightsDataClass>, val appendix: AppendixDataModel)

