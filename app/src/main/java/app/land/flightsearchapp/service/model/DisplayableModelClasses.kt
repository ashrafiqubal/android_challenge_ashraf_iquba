package app.land.flightsearchapp.service.model

data class AirlineDisplaybaleList(val list: List<AirlinesInfoModelClass>)

data class AirlinesInfoModelClass(
    val origin: String?,
    val destination: String?,
    val depTime: String?,
    val arrTime: String?,
    val airline: String?,
    val airlineCode: String?,
    val duration: String?,
    val lowestFare: String?,
    val type: String?,
    val fares: List<FaresModelClass>?,
    val rawData: FlightsDataClass?
)

data class FaresModelClass(val provider: String?, val fare: String)