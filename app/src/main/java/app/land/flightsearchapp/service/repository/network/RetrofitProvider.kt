package app.land.flightsearchapp.service.repository.network

import app.land.flightsearchapp.App
import app.land.flightsearchapp.BuildConfig
import com.google.gson.Gson
import okhttp3.ConnectionPool
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitProvider {

    private const val NETWORK_TIME_OUT: Long = 60L
    private const val BASE_URL: String = "http://www.mocky.io"

    fun getRetrofit(): Retrofit{
        val httpClient: OkHttpClient.Builder = OkHttpClient
            .Builder()
            .connectionPool(ConnectionPool())
            .readTimeout(NETWORK_TIME_OUT, TimeUnit.SECONDS)
            .connectTimeout(NETWORK_TIME_OUT, TimeUnit.SECONDS)

        if(BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(logging)
        }else{
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BASIC
            httpClient.addInterceptor(logging)
        }

        var builder: Retrofit.Builder = Retrofit.Builder()
        try {
            builder = builder.baseUrl(BASE_URL)
        } catch (e: Exception) {
            e.printStackTrace()
        }


        return builder
            .callbackExecutor(App.executors.networkIO())
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .client(httpClient.build())
            .build()
    }

}