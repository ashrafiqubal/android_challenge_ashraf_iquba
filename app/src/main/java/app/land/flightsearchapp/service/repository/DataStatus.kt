package app.land.flightsearchapp.service.repository

enum class DataStatus {
    LOADING,
    DB,
    NETWORK,
    FAILURE
}