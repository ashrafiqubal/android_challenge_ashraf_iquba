package app.land.flightsearchapp.service.repository.network

import app.land.flightsearchapp.service.model.SearchResultsDataModel
import retrofit2.Call
import retrofit2.http.GET

interface AirlinesServiceInterface {

    @GET("/v2/5979c6731100001e039edcb3")
    fun getAirlineList(): Call<SearchResultsDataModel>

}