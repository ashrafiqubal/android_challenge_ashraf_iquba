package app.land.flightsearchapp.service.repository

import androidx.lifecycle.MutableLiveData
import app.land.flightsearchapp.App
import app.land.flightsearchapp.service.model.SearchResultsDataModel
import app.land.flightsearchapp.service.repository.network.AirlinesServiceInterface
import app.land.flightsearchapp.service.repository.network.RetrofitProvider
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object AirlinesRepo {

    val service = RetrofitProvider.getRetrofit().create(AirlinesServiceInterface::class.java)

    fun getAirlineList(): MutableLiveData<Pair<DataStatus, SearchResultsDataModel?>> {

        val loadingPair = Pair(DataStatus.LOADING, null)
        val liveData: MutableLiveData<Pair<DataStatus, SearchResultsDataModel?>> = MutableLiveData()
        liveData.value = loadingPair
        val searchCall = service.getAirlineList()
        searchCall.enqueue(object : Callback<SearchResultsDataModel> {
            override fun onResponse(
                call: Call<SearchResultsDataModel>,
                response: Response<SearchResultsDataModel>
            ) {
                if (response.isSuccessful) {
                    val body = response.body()
                    val data = Pair(DataStatus.NETWORK, body)
                    App.executors.mainThread().execute {
                        liveData.value = data
                    }
                } else {
                    val data = Pair(DataStatus.FAILURE, null)
                    App.executors.mainThread().execute {
                        liveData.value = data
                    }
                }
            }

            override fun onFailure(call: Call<SearchResultsDataModel>, t: Throwable) {
                val data = Pair(DataStatus.FAILURE, null)
                App.executors.mainThread().execute {
                    liveData.value = data
                }
            }
        })

        return liveData
    }


}