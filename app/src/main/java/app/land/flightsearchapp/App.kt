package app.land.flightsearchapp

import android.app.Application
import app.land.flightsearchapp.utils.AppExecutors
// qRdyzxLug5muD9FhEDDE
object App {

    var application: Application? = null
    val executors = AppExecutors()

    fun init(app: Application){
        application = app
    }
}