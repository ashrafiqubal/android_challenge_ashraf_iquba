package app.land.flightsearchapp.utils

object Constants {

    object AirlinesCode{
        const val SPICE_JET_CODE = "SG"
        const val AIR_INDIA_CODE = "AI"
        const val GO_AIR_CODE = "G8"
        const val JET_AIRWAYS_CODE = "9W"
        const val INDIGO_CODE = "6E"
    }

}