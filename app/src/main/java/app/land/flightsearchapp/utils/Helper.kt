package app.land.flightsearchapp.utils

import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

object Helper {

    /**
     * Date format used to display arrival timing or departure timing
     * */
    private const val AIRLINES_DATE_FORMAT = "hh:mm"

    /**
     * Amount format, to format amount in indian standard format for better readability
     * */
    private const val AMOUNT_FORMAT = "##,##,###"

    /**
     * Rupee Symbol
     * */
    private const val INDIAN_RUPEE_SYMBOL = "₹"


    /**
     * Use this method to format time to display i.e. arrival time, dep time. It returns time in 24 hour format, without having date
     * */
    fun getFormattedTime(time: Long): String{
        var formattedTime = ""

        val formatter = SimpleDateFormat(AIRLINES_DATE_FORMAT, Locale.getDefault())

        formattedTime = formatter.format(Date(time))

        return formattedTime
    }

    /**
     * Format any amount in readable format, appended with rupee symbol
     * */
    fun getAmountString(amount: Int): String{
        val decimalFormat = DecimalFormat(AMOUNT_FORMAT).format(amount)
        return String.format("%s%s", INDIAN_RUPEE_SYMBOL, decimalFormat)
    }

    /**
     * get the difference between two time. It return difference in hours and minute e.g 48h 20m
     * */
    fun getDuration(starTime: Long, endTime: Long): String{
        val durationInSeconds = (endTime - starTime)/1000
        val hours = durationInSeconds/3600
        val remaining = durationInSeconds % 3600
        val minutes = remaining/60
        return String.format("%sh %sm", hours, minutes)
    }

}