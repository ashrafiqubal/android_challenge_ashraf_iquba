package app.land.flightsearchapp.viewmodel

import android.text.TextUtils
import androidx.annotation.WorkerThread
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import app.land.flightsearchapp.App
import app.land.flightsearchapp.service.model.AirlineDisplaybaleList
import app.land.flightsearchapp.service.model.AirlinesInfoModelClass
import app.land.flightsearchapp.service.model.FaresModelClass
import app.land.flightsearchapp.service.model.SearchResultsDataModel
import app.land.flightsearchapp.service.repository.AirlinesRepo
import app.land.flightsearchapp.service.repository.DataStatus
import app.land.flightsearchapp.utils.Helper

class AirlinesViewModel : ViewModel() {

    val displayableData: MutableLiveData<Pair<DataStatus, AirlineDisplaybaleList>> =
        MutableLiveData()

    fun getAirlinesDisplayList(lifecycleOwner: LifecycleOwner): MutableLiveData<Pair<DataStatus, AirlineDisplaybaleList>> {
        AirlinesRepo.getAirlineList().observe(lifecycleOwner, {
            App.executors.diskIO().execute {
                val rawData = it
                val convertedData = Pair(rawData.first, convertData(rawData.second))
                App.executors.mainThread().execute {
                    displayableData.value = convertedData
                }
            }
        })
        return displayableData
    }

    @WorkerThread
    private fun convertData(data: SearchResultsDataModel?): AirlineDisplaybaleList {
        val list = ArrayList<AirlinesInfoModelClass>()

        if (data != null) {
            val airports = data.appendix.airports
            val airlines = data.appendix.airlines
            val providers = data.appendix.providers

            for (flight in data.flights) {
                val origin = airports[flight.originCode]
                val destination = airports[flight.destinationCode]
                val depTime = Helper.getFormattedTime(flight.departureTime)
                val arrTime = Helper.getFormattedTime(flight.arrivalTime)
                val airline = airlines[flight.airlineCode]
                val fareList = ArrayList<FaresModelClass>()
                val lowestFareAmount = StringBuilder()
                for (fare in flight.fares) {
                    val provider = providers[fare.providerId.toString()]
                    val fareAmount = Helper.getAmountString(fare.fare)
                    if(TextUtils.isEmpty(lowestFareAmount.toString())){
                        lowestFareAmount.append("$fareAmount @ $provider")
                    }else{
                        lowestFareAmount.append("\n $fareAmount @ $provider")
                    }
                    fareList.add(FaresModelClass(provider, fareAmount))
                }

                val duration = Helper.getDuration(flight.departureTime, flight.arrivalTime)

                list.add(
                    AirlinesInfoModelClass(
                        origin,
                        destination,
                        depTime,
                        arrTime,
                        airline,
                        flight.airlineCode,
                        duration,
                        lowestFareAmount.toString(),
                        flight.type,
                        fareList,
                        flight
                    )
                )
            }
        }

        return AirlineDisplaybaleList(list)
    }

}